#инициализация библиотек
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import animation
from matplotlib.animation import FFMpegWriter
from IPython.display import HTML
from matplotlib.animation import FuncAnimation
import scipy.sparse.linalg as splinalg
from scipy.sparse import diags
matplotlib.use("Agg")

#Parameters of system
L=10#length
N=200#number of points for space
dx=L/N#space step
x=dx/2 + dx*np.arange(N)#massive for space
tm=150#max time
dt=0.65#time step
t=np.linspace(0,tm, int(tm/dt))#massive for time
nt=len(t)#number of points for time

#creation of pre-animation
fig = plt.figure()
ax = plt.axes(xlim=(0, 9), ylim=(-5, 5))
line, = ax.plot([], [], lw=4)

#frame updating
def init():
    line.set_data([], [])
    return line,

#initial potentional
def V(x):
    return (x-0.5*L)**2

#creation of hamiltonian
g=1./(dx*dx)
diagonal = g+V(x)
subdiagonal = -0.5*g*np.ones(N-1)
Msparse = diags([diagonal,subdiagonal,subdiagonal], [0, 1, -1])

#finding eigenvalues and eigenvectors
evals, evecs = splinalg.eigsh(Msparse, sigma=0, k=7)
print(evecs)

#initial wavefunction
def psi0(x):
    return np.exp(-(x-0.3*L)**2)

#eigenfunctions
phi=evecs/np.sqrt(dx)
#print(phi)
#
#коэффициенты разложения Cn
Cn=np.zeros(phi.shape[1])
for i in range(phi.shape[1]):#i-счетчик элемента
    Cn[i]=np.sum(np.conj(phi[:,i])*psi0(x)*dx)
#(phi[:,0].shape)
    
#wavefunction after expansion and multiplication on exp
#for j in range(N):
    #psiwave=np.zeros(phi[:,0].shape)
    #for i in range(phi.shape[1]):
        #psiwave=psiwave+phi[:,i]*Cn[i]*np.exp(-1j*evals[i]*j)

#creation of re-animation
def animate(j):
    psiwave=np.zeros(phi[:,0].shape)
    for i in range(phi.shape[1]):
        psiwave=psiwave+phi[:,i]*Cn[i]*np.exp(-1j*evals[i]*j)
    line.set_data(x, psiwave*np.conj(psiwave))
    return line,

#final animation
anima = FuncAnimation(fig, animate, init_func=init,
                                  frames=80, interval=120, blit=True)
FFwriter=FFMpegWriter(fps=10, metadata=dict(artist='Me'), bitrate=1800)
HTML(anima.to_html5_video())
anima.save('psiwave3.mp4', writer=FFwriter)
